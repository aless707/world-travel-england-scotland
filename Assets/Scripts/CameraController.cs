﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform player;

    public float left;
    public float right;

    void Update()
    {
        transform.position = new Vector3(Mathf.Clamp(player.position.x, left, right), transform.position.y, transform.position.z);
    }
}

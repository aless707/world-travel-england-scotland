﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Score : Singleton<Score>
{
    protected Score() { }

    private TextMeshProUGUI scoreText;
    private int points;
    public int Points
    {
        get
        {
            return points;
        }
        set
        {
            points = value;
            scoreText.text = ToString();
            if (points == maxPoints)
            {
                SouvenirGot.Instance.GiveReward();
            }
        }
    }
    public int maxPoints;

    void Start()
    {
        scoreText = GetComponent<TextMeshProUGUI>();
        points = 0;
    }

    public override string ToString()
    {
        return points + "/" + maxPoints;
    }
}

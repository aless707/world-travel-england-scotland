﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EyeBlink : MonoBehaviour
{
    public float blinkEyeRate;
    private Animator anim;
    private float previousBlinkEyeRate;
    private float blinkEyeTime;
    void Update()
    {
        anim = gameObject.GetComponent<Animator>();
        if (Time.time > blinkEyeTime)
        {
            previousBlinkEyeRate = blinkEyeRate;
            blinkEyeTime = Time.time + blinkEyeRate;           
            anim.SetTrigger("blink");

            while (previousBlinkEyeRate == blinkEyeRate)
            {
                blinkEyeRate = Random.Range(4f, 10f);
            }
        }
    }
}

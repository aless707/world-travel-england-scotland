﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Audio;

public class MainMenuControl : MonoBehaviour
{

    public GameObject newgamePanel;
    public GameObject settingsPanel;
    public GameObject loadgameButton;
    public GameObject helpPanel;
    public GameObject souvenirPanel;
    public GameObject quitgamePanel;
    private int sceneToContinue;
    public AudioMixer musicMixer;
    public Animator musicAnim;
    public Slider Mslider;
    public Slider Sslider;
    public Animator transitionAnim;
    public Toggle fullscreen;
    public GameObject resetPanel;


    void Start()
    {
        CheckLoad();
    }

    private void CheckLoad()
    {
        if (!string.IsNullOrEmpty(PlayerPrefs.GetString("saveData")))
        {
            loadgameButton.GetComponent<Button>().interactable = true;
        }
        else
        {
            loadgameButton.GetComponent<Button>().interactable = false;
        }
        bool.TryParse(PlayerPrefs.GetString("fullscreen"), out bool result);
        fullscreen.isOn = result;

        Mslider.value = PlayerPrefs.GetFloat("musicvolume");
        Sslider.value = PlayerPrefs.GetFloat("sfxvolume");
        SouvenirController.Instance.UpdateStatus();

    }

    public void NewGame()
    {
        if (sceneToContinue != 0)
        {
            settingsPanel.SetActive(false);
            helpPanel.SetActive(false);
            souvenirPanel.SetActive(false);
            resetPanel.SetActive(false);
            newgamePanel.SetActive(true);
        }
        else
        {
            StartCoroutine(ChangeScene());
        }
    }

    public void LoadGame()
    {
        Loader.Instance.LoadSave();
    }

    public void QuitGame()
    {
        helpPanel.SetActive(false);
        newgamePanel.SetActive(false);
        souvenirPanel.SetActive(false);
        settingsPanel.SetActive(false);
        resetPanel.SetActive(false);
        quitgamePanel.SetActive(true);
    }

    public void NewGameYes()
    {
        StartCoroutine(ChangeScene());
    }

    public void QuitGameYes()
    {
        Application.Quit();
    }

    public void QuitGameNo()
    {
        quitgamePanel.SetActive(false);
    }

    public void NewGameNo()
    {
        newgamePanel.SetActive(false);
    }

    public void SetVolumeM(float music)
    {
        PlayerPrefs.SetFloat("musicvolume", music);
        musicMixer.SetFloat("musicvolume", music);
    }

    public void SetVolumeS(float sfx)
    {
        PlayerPrefs.SetFloat("sfxvolume", sfx);
        musicMixer.SetFloat("sfxvolume", sfx);
    }

    public void SetFullScreen(bool isFullscreen)
    {
        Screen.fullScreen = isFullscreen;
        PlayerPrefs.SetString("fullscreen", isFullscreen.ToString());
    }

    public void Settings()
    {
        helpPanel.SetActive(false);
        newgamePanel.SetActive(false);
        souvenirPanel.SetActive(false);
        quitgamePanel.SetActive(false);
        resetPanel.SetActive(false);
        settingsPanel.SetActive(true);
    }

    public void Help()
    {
        settingsPanel.SetActive(false);
        newgamePanel.SetActive(false);
        souvenirPanel.SetActive(false);
        quitgamePanel.SetActive(false);
        resetPanel.SetActive(false);
        helpPanel.SetActive(true);
    }

    public void Souvenirs()
    {
        settingsPanel.SetActive(false);
        newgamePanel.SetActive(false);
        helpPanel.SetActive(false);
        quitgamePanel.SetActive(false);
        resetPanel.SetActive(false);
        souvenirPanel.SetActive(true);
    }

    IEnumerator ChangeScene()
    {
        musicAnim.SetTrigger("fadeOut");
        transitionAnim.SetTrigger("end");
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene(1);
    }

    IEnumerator ChangeSavedScene()
    {
        musicAnim.SetTrigger("fadeOut");
        transitionAnim.SetTrigger("end");
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene(sceneToContinue);
    }

    public void ResetProgress()
    {
        settingsPanel.SetActive(false);
        helpPanel.SetActive(false);
        souvenirPanel.SetActive(false);
        newgamePanel.SetActive(false);
        resetPanel.SetActive(true);
    }

    public void ResetNo()
    {
        resetPanel.SetActive(false);
    }

    public void ResetYes()
    {
        PlayerPrefs.DeleteAll();
        resetPanel.SetActive(false);
        CheckLoad();
    }
}

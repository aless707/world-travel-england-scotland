﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using static LevelControlScript;

public class PauseMenuControl : MonoBehaviour
{
    public GameObject savegamePanel;
    public GameObject settingsPanel;
    public GameObject loadgameButton;
    public GameObject helpPanel;
    public GameObject souvenirPanel;
    public GameObject quitgamePanel;
    public GameObject mainmenuPanel;
    private int sceneToContinue;
    private int currentSceneIndex;
    public Toggle fullscreen;
    public Animator transitionAnim;
    public Animator musicAnim;

    [Header("Items to save")]
    public GameObject player;
    public Score score;
    public List<LevelControlScript> questions;

    void Start()
    {
        bool.TryParse(PlayerPrefs.GetString("fullscreen"), out bool result);
        fullscreen.isOn = result;
        sceneToContinue = PlayerPrefs.GetInt("SavedScene");

        CheckLoad();
    }

    private void CheckLoad()
    {
        if (!string.IsNullOrEmpty(PlayerPrefs.GetString("saveData")))
        {
            loadgameButton.GetComponent<Button>().interactable = true;
        }
        else
        {
            loadgameButton.GetComponent<Button>().interactable = false;
        }
    }

    public void SaveGame()
    {
        helpPanel.SetActive(false);
        quitgamePanel.SetActive(false);
        souvenirPanel.SetActive(false);
        settingsPanel.SetActive(false);
        mainmenuPanel.SetActive(false);
        savegamePanel.SetActive(true);
    }

    public void SaveGameYes()
    {
        currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        PlayerPrefs.SetInt("SavedScene", currentSceneIndex);
        savegamePanel.SetActive(false);

        LevelControlScript[] questions = Resources.FindObjectsOfTypeAll<LevelControlScript>();
        PlayerController[] player = Resources.FindObjectsOfTypeAll<PlayerController>();
        Score[] score = Resources.FindObjectsOfTypeAll<Score>();

        List<string> correct = new List<string>();
        List<string> failed = new List<string>();

        for (int i = 0; i < questions.Length; i++)
        {
            switch (questions[i].status)
            {
                case QuestionStatus.Correct:
                    correct.Add(questions[i].name);
                    break;
                case QuestionStatus.Incorrect:
                    failed.Add(questions[i].name);
                    break;
                default:
                    break;
            }
        }

        var save = new SaveData()
        {
            scene = SceneManager.GetActiveScene().buildIndex,
            XPos = player[0].gameObject.transform.position.x,
            score = score[0].Points,
            correctList = correct,
            failedList = failed
        };

        PlayerPrefs.SetString("saveData", save.ToString());

        gameObject.SetActive(false);
        Time.timeScale = 1f;
        CheckLoad();
    }

    public void SaveGameNo()
    {
        savegamePanel.SetActive(false);
    }

    public void LoadGame()
    {
        Loader.Instance.LoadSave();
    }

    public void QuitGame()
    {
        helpPanel.SetActive(false);
        savegamePanel.SetActive(false);
        souvenirPanel.SetActive(false);
        settingsPanel.SetActive(false);
        mainmenuPanel.SetActive(false);
        quitgamePanel.SetActive(true);
    }

    public void QuitGameNo()
    {
        quitgamePanel.SetActive(false);
    }

    public void QuitGameYes()
    {
        Application.Quit();
    }

    public void MainMenu()
    {
        helpPanel.SetActive(false);
        savegamePanel.SetActive(false);
        souvenirPanel.SetActive(false);
        settingsPanel.SetActive(false);
        quitgamePanel.SetActive(false);
        mainmenuPanel.SetActive(true);
    }

    public void MainMenuNo()
    {
        mainmenuPanel.SetActive(false);
    }

    public void MainMenuYes()
    {
        StartCoroutine(ChangeMenuScene());
    }

    public void SetFullScreen(bool isFullscreen)
    {
        Screen.fullScreen = isFullscreen;
    }

    public void Settings()
    {
        helpPanel.SetActive(false);
        savegamePanel.SetActive(false);
        souvenirPanel.SetActive(false);
        quitgamePanel.SetActive(false);
        mainmenuPanel.SetActive(false);
        settingsPanel.SetActive(true);
    }

    public void Help()
    {
        settingsPanel.SetActive(false);
        savegamePanel.SetActive(false);
        souvenirPanel.SetActive(false);
        quitgamePanel.SetActive(false);
        mainmenuPanel.SetActive(false);
        helpPanel.SetActive(true);
    }

    public void Souvenirs()
    {
        settingsPanel.SetActive(false);
        savegamePanel.SetActive(false);
        helpPanel.SetActive(false);
        quitgamePanel.SetActive(false);
        mainmenuPanel.SetActive(false);
        souvenirPanel.SetActive(true);
    }

    IEnumerator ChangeScene()
    {
        musicAnim.SetTrigger("fadeOut");
        transitionAnim.SetTrigger("end");
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene(1);
    }

    IEnumerator ChangeMenuScene()
    {
        Time.timeScale = 1f;
        musicAnim.SetTrigger("fadeOut");
        transitionAnim.SetTrigger("end");
        yield return new WaitForSecondsRealtime(2);
        SceneManager.LoadScene(0);
    }

    public class SaveData
    {
        public int scene;
        public float XPos;
        public int score;
        public List<string> failedList;
        public List<string> correctList;

        public override string ToString()
        {
            return JsonUtility.ToJson(this);
        }
    }
}

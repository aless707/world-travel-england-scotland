﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuExitPanel : MonoBehaviour
{
    public GameObject Panel;
    public AudioSource UIsource;
    public AudioClip click;

    public void OnMouseDown()
    {
        UIsource.clip = click;
        UIsource.Play();

        Panel.SetActive(false);
    }
}

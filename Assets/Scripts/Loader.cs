﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using static PauseMenuControl;

public class Loader : Singleton<Loader>
{
    public Animator transitionAnim;
    public Animator musicAnim;

    public void LoadSave()
    {
        StartCoroutine(ChangeSavedScene());
    }

    IEnumerator ChangeSavedScene()
    {
        //Load SaveData
        var save = JsonUtility.FromJson<SaveData>(PlayerPrefs.GetString("saveData"));
        //Create async operation
        AsyncOperation op = SceneManager.LoadSceneAsync(save.scene, LoadSceneMode.Additive);
        //Do not activate automatically
        op.allowSceneActivation = false;
        //New scene reference
        //Scene newlyLoadedScene = SceneManager.GetSceneAt(SceneManager.sceneCount - 1);

        //Resume time because of WaitForSeconds
        Time.timeScale = 1f;
        //Play fade out
        musicAnim.SetTrigger("fadeOut");
        transitionAnim.SetTrigger("end");
        yield return new WaitForSeconds(2);

        //Allow new scene to activate
        op.allowSceneActivation = true;
        //Wait for scene to load
        yield return op;

        //Do not destroy current script
        DontDestroyOnLoad(this);
        //Wait for old scene to unload
        yield return SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene().buildIndex);

        //Load all save data into new scene
        PlayerController[] player = Resources.FindObjectsOfTypeAll<PlayerController>();
        Score[] score = Resources.FindObjectsOfTypeAll<Score>();

        //Player position
        player[0].transform.position = new Vector3(save.XPos, player[0].transform.position.y, player[0].transform.position.z);
        //Player score
        //score[0].Points = save.score;
        //Load all correct answers
        List<LevelControlScript> questions = new List<LevelControlScript>(Resources.FindObjectsOfTypeAll<LevelControlScript>());

        foreach (string question in save.correctList)
        {
            questions.First(x => x.name.Equals(question)).RightAnswer();
        }
        //Load all incorrect answers
        foreach (string question in save.failedList)
        {
            questions.First(x => x.name.Equals(question)).WrongAnswer(0);
        }

        //Destroy loader when it finished its jobz
        Destroy(gameObject);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelOpener : MonoBehaviour
{
    public GameObject Panel;
    public AudioSource select;
    public AudioSource UIsource;
    public AudioClip click;

    public Collider2D touchCollider;
    public Collider2D touchCollider2;
    public Collider2D touchCollider3;

    public void OpenPanel()
    {
        UIsource.clip = click;
        UIsource.Play();

        Panel.SetActive(false);

        Time.timeScale = 1f;

        touchCollider.enabled = true;
        touchCollider2.enabled = true;
        touchCollider3.enabled = true;

        select.mute = false;
    }
}

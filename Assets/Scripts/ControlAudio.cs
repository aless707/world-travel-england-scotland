﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class ControlAudio : MonoBehaviour
{
    public AudioMixer musicMixer;
    public Slider Mslider;
    public Slider Sslider;

    void Start()
    {
        Mslider.value = PlayerPrefs.GetFloat("musicvolume");
        Sslider.value = PlayerPrefs.GetFloat("sfxvolume");
    }

    public void SetVolumeM(float music)
    {
        PlayerPrefs.SetFloat("musicvolume", music);
        musicMixer.SetFloat("musicvolume", music);
    }

    public void SetVolumeS(float sfx)
    {
        PlayerPrefs.SetFloat("sfxvolume", sfx);
        musicMixer.SetFloat("sfxvolume", sfx);
    }
}

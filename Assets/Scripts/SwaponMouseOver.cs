﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwaponMouseOver : MonoBehaviour
{
    public Sprite sprite1;
    public Sprite sprite2;
    public GameObject Panel;
    public AudioSource select;
    public Texture2D cursorMain;
    public Texture2D cursorSelect;
    public CursorMode cursorMode = CursorMode.Auto;
    public Vector2 hotSpot = Vector2.zero;

    public Collider2D touchCollider;
    public Collider2D touchCollider2;
    public Collider2D touchCollider3;
    public Collider2D dingCollider;

    private SpriteRenderer spriteRenderer;

    void Start()
    {
        select = GetComponent<AudioSource>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        if (spriteRenderer.sprite == null)
            spriteRenderer.sprite = sprite1;
    }

    void OnMouseEnter()
    {
        ChangeSprite();
    }

    void OnMouseExit()
    {
        ChangeSprite();
    }

    public void ChangeSprite()
    {
        if (spriteRenderer.sprite == sprite1)
        {
            spriteRenderer.sprite = sprite2;
            select.Play();
            Cursor.SetCursor(cursorSelect, hotSpot, cursorMode);

        }
        else
        {
            spriteRenderer.sprite = sprite1;
            Cursor.SetCursor(cursorMain, hotSpot, cursorMode);
        }
    }

    public void OnMouseDown()
    {
        if (Panel != null)
        {
            Panel.SetActive(true);

            dingCollider.enabled = false;
            touchCollider.enabled = false;
            touchCollider2.enabled = false;
            touchCollider3.enabled = false;

            Time.timeScale = 0f;

            select.mute = true;

        }
    }
}

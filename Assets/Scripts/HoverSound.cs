﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoverSound : MonoBehaviour
{
    public AudioSource hoverSound;
    public AudioSource clickSound;

    public void HoverSfx()
    {
        hoverSound.Play();
    }

    public void ClickSfx()
    {
        clickSound.Play();
    }

}

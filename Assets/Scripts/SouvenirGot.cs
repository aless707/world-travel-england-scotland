﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SouvenirGot : Singleton<SouvenirGot>
{
    protected SouvenirGot() { }

    public GameObject souPanel;
    public string souName;

    public void GiveReward()
    {
        souPanel.SetActive(true);
        PlayerPrefs.SetInt(souName, 1);
        StartCoroutine(CloseDelay());
        SouvenirController.Instance.UpdateStatus();
    }

    public IEnumerator CloseDelay()
    {
        yield return new WaitForSecondsRealtime(3f);
        souPanel.SetActive(false);
    }
}

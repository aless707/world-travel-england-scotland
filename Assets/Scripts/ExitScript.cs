﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ExitScript : MonoBehaviour
{
    public GameObject exitPanel;
    public Animator musicAnim;
    public int scene;
    public Animator transitionAnim;

    void OnTriggerEnter2D()
    {
        exitPanel.SetActive(true);

        Time.timeScale = 0f;
    }

    public void NotReady()
    {
        exitPanel.SetActive(false);

        Time.timeScale = 1f;
    }

    public void Ready()
    {
        StartCoroutine(ChangeScene());
    }

    IEnumerator ChangeScene()
    {
        Time.timeScale = 1f;
        musicAnim.SetTrigger("fadeOut");
        transitionAnim.SetTrigger("end");
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene(scene);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NotificationSound : MonoBehaviour
{
    public AudioSource notif;
    private bool hasPlayed = false;

    void Start()
    {
        notif = GetComponent<AudioSource> ();
    }

    void OnTriggerEnter2D ()
    {
        if (hasPlayed == false)
        {
            notif.Play ();
            hasPlayed = true;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelControlScript : MonoBehaviour
{
    public AudioSource src;
    public AudioClip win;
    public AudioClip lose;
    public GameObject correctAnswer;
    public List<GameObject> wrongAnswer;
    public float delay;
    public QuestionStatus status = QuestionStatus.Default;

    public Collider2D touchCollider;
    public Collider2D touchCollider2;
    public Collider2D touchCollider3;

    public void RightAnswer()
    {
        correctAnswer.GetComponent<Image>().color = new Color(0, 255, 0);
        Score.Instance.Points += 1;
        src.clip = win;
        touchCollider.enabled = true;
        touchCollider2.enabled = true;
        touchCollider3.enabled = true;
        if (isActiveAndEnabled)
        {
            src.Play();
            StartCoroutine(CloseDelay());
        }
        DisableAllButtons();
        status = QuestionStatus.Correct;
    }
    
    public void WrongAnswer(int ID)
    {
        wrongAnswer[ID].GetComponent<Image>().color = new Color(255, 0, 0);
        src.clip = lose;
        touchCollider.enabled = true;
        touchCollider2.enabled = true;
        touchCollider3.enabled = true;
        if (isActiveAndEnabled)
        {
            src.Play();
            StartCoroutine(CloseDelay());
        }
        DisableAllButtons();
        status = QuestionStatus.Incorrect;
    }

    public IEnumerator CloseDelay()
    {
        float current = 0f;

        while(current < delay)
        {
            current += 0.1f;
            yield return new WaitForSecondsRealtime(0.1f);
        }

        gameObject.SetActive(false);
        Time.timeScale = 1f;
    }

    private void DisableAllButtons()
    {
        correctAnswer.GetComponent<Button>().interactable = false;
        foreach(GameObject item in wrongAnswer)
        {
            item.GetComponent<Button>().interactable = false;
        }
    }

    public enum QuestionStatus
    {
        Default,
        Correct,
        Incorrect
    }
}

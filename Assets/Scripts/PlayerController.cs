﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float currentSpeed;
    private Rigidbody2D rb;
    private Vector2 move;
    bool facingRight = true;
    public float left;
    public float right;
    public float walkSpeed;
    public float runSpeed;
    private float lastTap = 0f;
    private float lastDirection = 0f;
    private int tapCount = 0;

    Animator anim;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        currentSpeed = walkSpeed;
    }

    private void FixedUpdate()
    {
        float currentDirection = Input.GetAxisRaw("Horizontal");
        if (Input.GetKey(KeyCode.LeftShift))
        {
            currentSpeed = runSpeed;
        }
        else
        {
            currentSpeed = walkSpeed;
        }
        if(currentDirection == 0)
        {
            currentSpeed = 0;
        }

        Vector2 move2 = new Vector2(currentDirection, 0);
        move = move2.normalized * currentSpeed;

        anim.SetFloat("Speed", currentSpeed);

        rb.MovePosition(rb.position + move * Time.fixedDeltaTime);

        if (move2.x > 0 && !facingRight)
        {
            Flip();
        }
        else if (move2.x < 0 && facingRight)
        {
            Flip();
        }

        transform.position = new Vector2(Mathf.Clamp(transform.position.x, left, right), transform.position.y);

    }

    void Flip()
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    void PlaySound()
    {
        GetComponent<AudioSource>().Play();
    }
}

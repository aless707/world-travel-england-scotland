﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class End : MonoBehaviour
{
    public Animator musicAnim;
    public Animator transitionAnim;
    public int scene;


    void Start()
    {
        StartCoroutine(ChangeScene());
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            StartCoroutine(Skip());
        }
    }

    IEnumerator ChangeScene()
    {
        yield return new WaitForSeconds(57);
        musicAnim.SetTrigger("fadeOut");
        transitionAnim.SetTrigger("end");
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene(scene);
    }

    IEnumerator Skip()
    {
        musicAnim.SetTrigger("fadeOut");
        transitionAnim.SetTrigger("end");
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene(scene);
    }
}

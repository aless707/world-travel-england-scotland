﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SouvenirController : Singleton<SouvenirController>
{
    protected SouvenirController() { }

    public List<SouvenirPair> souvenirList;

    void Start()
    {
        UpdateStatus();
    }

    public void UpdateStatus()
    {
        foreach (SouvenirPair pair in souvenirList)
        {
            if (PlayerPrefs.GetInt(pair.name) == 1)
            {
                pair.prize.SetActive(true);
            }
        }
    }

    [Serializable]
    public class SouvenirPair
    {
        public string name;
        public GameObject prize;
    }
}
